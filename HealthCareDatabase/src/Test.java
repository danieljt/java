/**
 * Test class for implementations
 * @author daniel
 *
*/
public class Test {
	public static void main(String[] args) throws Exception{
		Drug aids = new Drug(0, "Aids");
		Drug canc = new Drug(1, "Cancer");
		Drug herp = new Drug(2, "Herpes");
		
		Patient bob = new Patient(0, "Bob", "Johnson", "Chillstreet", 22, 1111);
		Doctor tim = new Doctor(0, "Tim", "Tommy");
			
		Prescription one = new Prescription(0, 3, aids, tim, bob);
		Prescription two = new Prescription(1, 3, canc, tim, bob);
		Prescription thr = new Prescription(2, 4, herp, tim, bob);
		
		FIFOprescriptionList fifo = new FIFOprescriptionList();
		LIFOprescriptionList lifo = new LIFOprescriptionList();
		
		fifo.add(one);
		fifo.add(two);
		fifo.add(thr);
               	
		lifo.add(one);
		lifo.add(two);
		lifo.add(thr);
		
	}

}

