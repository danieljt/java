import java.util.Scanner;
import java.io.File;
/**
 * Class functions as a main program for the healthcare database. The class displays
 * an interactive menu for registering, changing and deleting information from
 * the database. This class is for testing only, and user of the database should
 * use the graphical user interface.
 * @author Daniel james Tarplett
 *
 */
public class Main {
	
	static int patientID = 0;
	static int drugID = 0;
	static int doctorID = 0;
	static int prescriptionID = 0;
	
	// Main database lists
	static Tabel<Patient> patients = new Tabel<Patient>(100);
	static Tabel<Drug> drugs = new Tabel<Drug>(100);
	static SortedList<Doctor> doctors = new SortedList<Doctor>();
	static LIFOprescriptionList prescriptions = new LIFOprescriptionList();
	
	/**
	 * Main method
	 * @param args
	 */
	public static void main(String[] args)throws Exception {

		readFile("databasefile.txt");
	}

	/**
	 * Reads database information from a save file to the main program
	 */
	static void readFile(String filename) throws Exception{
		File file = new File(filename);
		Scanner reader = new Scanner(file);
		String line = null;
		String[] words = null;
		
		// Read file line by line and sort according
		// to the different prefixes
		while(reader.hasNextLine()){
			line = reader.nextLine();
			
			// Add persons to the list
			if(line.startsWith("# Persons")){
				line = reader.nextLine();
				while(line.isEmpty() == false){					
					words = line.split(", ");
					patients.add(new Patient(Integer.parseInt(words[0]), words[1], words[2], words[3],
							Integer.parseInt(words[4]), Integer.parseInt(words[5])), 
							Integer.parseInt(words[0]));
					patientID += 1;
					line = reader.nextLine();									
				}
			}
			
			// Add drugs to the list
			else if(line.startsWith("# Drugs")){
				line = reader.nextLine();
				while(line.isEmpty() == false){
					words = line.split(", ");
					line = reader.nextLine();
					// Narcotic mixture
					if(words[2].equals("mixture") && words[3].equals("a")){
						drugs.add(new NarcoticMixture(Integer.parseInt(words[0]), words[1],
								Double.parseDouble(words[4]), Integer.parseInt(words[7]),
								Double.parseDouble(words[5]), Double.parseDouble(words[6])), 
								Integer.parseInt(words[0]));
					}
					
					// Narcotic pills
					else if (words[2].equals("pills") && words[3].equals("a")){
						drugs.add(new NarcoticPills(Integer.parseInt(words[0]), words[1],
								Double.parseDouble(words[4]), Integer.parseInt(words[7]),
								Integer.parseInt(words[5]), Double.parseDouble(words[6])),
								Integer.parseInt(words[0]));
					}
					
					// Addictive mixture
					else if(words[2].equals("mixture") && words[3].equals("b")){
						drugs.add(new AddictiveMixture(Integer.parseInt(words[0]), words[1],
								Double.parseDouble(words[4]), Integer.parseInt(words[7]),
								Double.parseDouble(words[5]), Double.parseDouble(words[6])), 
								Integer.parseInt(words[0]));
					}
					
					// Addictive pills
					else if(words[2].equals("pills") && words[3].equals("b")){
					drugs.add(new AddictivePills(Integer.parseInt(words[0]), words[1],
							Double.parseDouble(words[4]), Integer.parseInt(words[7]),
							Integer.parseInt(words[5]), Double.parseDouble(words[6])), 
							Integer.parseInt(words[0]));
					}
					
					// Regular mixture
					else if(words[2].equals("mixture") && words[3].equals("c")){
						drugs.add(new RegularMixture(Integer.parseInt(words[0]), words[1],
								Double.parseDouble(words[4]), Double.parseDouble(words[5]),
								Double.parseDouble(words[6])), 
								Integer.parseInt(words[0]));
					}
					
					// regular pills
					else if(words[2].equals("pills") && words[3].equals("c")){
						drugs.add(new RegularPills(Integer.parseInt(words[0]), words[1],
								Double.parseDouble(words[4]), Integer.parseInt(words[5]),
								Double.parseDouble(words[6])), 
								Integer.parseInt(words[0]));
					}
					
				}
				
			}
			
			// Add doctors to the list
			else if(line.startsWith("# Doctors")){
				line = reader.nextLine();
				while(line.isEmpty() == false){
					words = line.split(", ");
					
					// Add regular doctor
					if(Integer.parseInt(words[3]) == 0){
						doctors.add(new Doctor(Integer.parseInt(words[0]), words[1], words[2]));
					}
					
					// Add general practitioner
					else{
						doctors.add(new GeneralPractitioner(Integer.parseInt(words[0]), words[1],
								words[2], Integer.parseInt(words[3])));
					}
					line = reader.nextLine();
				}
			}
			
			// Add prescriptions to the list
			else if(line.startsWith("# Prescriptions")){
				line = reader.nextLine();
				while(line.isEmpty() == false){
					words = line.split(", ");
					
					// Get doctor, patient and drug from lists
					Patient patient = patients.find(Integer.parseInt(words[2]));
					Drug drug = drugs.find(Integer.parseInt(words[4]));
					//Doctor doctor = doctors.find();
					line = reader.nextLine();
				}
			}
			
			// End the reader
			if(line.startsWith("# End")){
				break;
			}
		}
		reader.close();
	}
	
	/**
	 * Writes database information from the database to a save file. If no
	 * save file is specified, the program writes information to the main
	 * save file 
	 */
	static void writeToFile(){
		
	}
	
	/**
	 * Adds a new patient to the database and assigns a doctor to the patient
	 */
	static void newPatient(){
		
	}
	
	/**
	 * Adds a new doctor to the database
	 */
	static void newDoctor(){
		
	}
	
	/**
	 * Adds a new drug to the database
	 */
	static void newDrug(){
		
	}
	
	/**
	 * Adds a new prescription
	 */
	static void newPrescription(){
		
	}
}
