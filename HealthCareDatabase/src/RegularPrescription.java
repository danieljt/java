
/**
 * Class for regular prescriptions without narcotic or addictive properties
 * @author Daniel James Tarplett
 *
 */
public class RegularPrescription extends Prescription{
       
       public RegularPrescription(int id, int uses, Drug drug, Doctor doctor, Patient patient){
               super(id, uses, drug, doctor, patient);
               // Calculate subsidized price
               this.price = drug.price;
       }
}
