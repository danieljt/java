
/**
   Class for holding information about a DVD and its owner.
   Also holds information about who is borrowing the DVD
 */

public class DVD
{
  private String title;
  private Person owner;
  private Person borrower;

  public DVD(String title_in, Person owner_in)
  {
    // Set the title, the owner and set the
    // borrowed status to null when initialized
    title = title_in;
    owner = owner_in;
    borrower = null;
  }

  /**
    Method for returning the title of the
    dvd in string format
  */

  public String title()
  {
    return title;
  } 

  /**
     Method for returning the owner of the dvd
     in string format
  */
  public Person owner()
  {
    return owner;
  }
 
  /**
     Method for returning the borrowed status of the
     dvd. Returns the current person borrowing it. Returns
     null if it is not borrowed 
   */
  public Person borrower()
  {
    return borrower;
  }

  /**
     Method registering the borrower of the dvd, returns null
     if the dvd is not being borrowed
   */
  public void move(Person name)
  {
    borrower = name;
  }
}

