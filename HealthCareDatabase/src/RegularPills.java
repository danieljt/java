/**
 * Class for defining a package of pills for a regular drug without strong
 * substances.
 * @author daniel
 *
 */
public class RegularPills extends RegularDrug{
       protected int quantity;
       protected double substance;
       
       public RegularPills(int id, String name, double price, int quantity,
                       double substance){
               super(id, name);
               this.price = price;
               this.quantity = quantity;
               }
               
       /**
        * Returns the amount of pills in this package
        * @return amount of pills
        */
       public int quantity(){
               return quantity;
               }
       
       /**
        * Returns the price of the box
        * @return price
        */
       public double price(){
               return price;
               }
}
