/**
 * Class for defining a linked item list. The list takes a list of
 * types as an argument and sorts after that criteria. The second
 * criteria is alphabetically. 
 * @author Daniel James Tarplett
 *
 */
public class ItemList {
	public Item[] list;
	public Node head;
	
	public ItemList(Item[] list){
		this.list = list;
	}
	
	/**
	 * Method adds an item to the list. The method sorts with respect
	 * to the types contained in the input list, and after that the 
	 * list sorts alphabetically
	 */
	public void add(Item item){
		
	    // Empty list
	    if(head == null){
		head.item = item;
	    }
	    // Iterate rest of the list and add in the appropriate order
	    else{
		Node current = head;
		while(current.next != null){
		    if(current.item.largerThan(current.next.item) == true && 
			    current.item.getName().compareTo(current.next.item.getName()) > 0){
		    }
		}
	    }
	}
	
	/**
	 * Method compares item one with item two and returns true if 
	 * the sorting parameter of one is larger than two. if not,
	 * it returns false.
	 * 
	 * @return boolean
	 */
	public boolean largerThan(Item one, Item two){
	    if(one.getSortingParameter() > two.getSortingParameter()){
		return true;
	    }
	    else{
		return false;
	    }
	}
	
	/**
	 * The node class holds items to be used by a linked list
	 * @author Daniel James Tarplett
	 *
	 */
	private class Node{
	    Item item;
	    Node next;
	    
	    public Node(Item item){
		this.item = item;
	    }
	}
}
