import java.util.HashMap;
import java.util.Scanner;
import java.io.File;
import java.io.PrintWriter;

public class DVDAdmin
{
  public static void main(String[] args) throws Exception 
  {
    HashMap<String, Person> names = new HashMap<>();
    readarchive(names);
    menu(names);
    writearchive(names);

  }

  /**
     Method for displaying the interactive menu session. The method gives 
     a number of choices to the user and applies the information
     to the archive
   */
  static void menu(HashMap<String, Person> names)
  {
        Scanner input = new Scanner(System.in);



    // Define the choice variable
    int choice;

    // Make the choice, update the classes.
    do
      {
	// Display menu
	System.out.println("------------------------------");
	System.out.println("MENY FOR DVD ADMINISTRASJON");
	System.out.println("------------------------------");
	System.out.println();
	System.out.println("VENNLIGST VELG EN HANDLING:");
	System.out.println();
	System.out.println("1: Register a new person");
	System.out.println("2: Buy film");
	System.out.println("3: Borrow film");
	System.out.println("4: Show");
	System.out.println("5: Overview of customers");
	System.out.println("6: Return film");
	System.out.println("7: Exit");
	System.out.println("------------------------------");
	System.out.println();

	// Test that the input is integer
	while(true)
	  {
	    System.out.print("Make a choice: ");
	    try
	      {		
		choice = Integer.parseInt(input.nextLine());
		break;
	      }
	    catch(NumberFormatException ignore)
	      {
		System.out.println("Input not valid!");
		System.out.println("Input must be an integer!");
	      }
	  }
	
	// Add person
	if(choice == 1)
	  {
	    // Give the name of the person
	    System.out.print("Name: ");
	    String name = input.nextLine();
	    Person registered = names.get(name);

	    // Check if person is registered, add if not
	    if(registered == null)
	      {
	        names.put(name, new Person(name));
	      }
	    else
	      {
		System.out.println(registered.name() + " is already registered");
	      }
	    System.out.println();
	  }

	// Register a purchase
	else if(choice == 2)
	  {
	    // Read the name and check if person is registered
	    System.out.print("Name of buyer: ");
	    String name = input.nextLine();
	    Person buyer = names.get(name);
	    if(buyer == null)
	      {
		System.out.println(name + " is not registered in the database!");
	      }
	    else
	      {
		// Buy the DVD if the person doesn't have it
		System.out.print("The title of the DVD: ");
		String title = input.nextLine();
		DVD bought = new DVD(title, buyer);
		buyer.buy(bought);
	      }
	    System.out.println();
	  }

	else if(choice == 3)
	  {
	    // Read the name of the lendier
	    System.out.print("Give the name of the borrower: ");
	    String name1 = input.nextLine();
	    Person borrower = names.get(name1);

	    // Check if lender exists
	    if(borrower == null)
	      {
		System.out.println(name1 + " is not registered in the database!");
	      }
	    else
	      {
		// Read the name of the person lending
		System.out.print("Give the name of the lender: ");
		String name2 = input.nextLine();
		Person lender = names.get(name2);

		// Check if the person lending exists
		if(lender == null)
		  {
		    System.out.println(name2 + " is not registered in the database!");
		  }
		else if(lender == borrower)
		  {
		    System.out.println(name1 + " cannot borrow a DVD of him/herself!");
		  }
		else
		  {
		    // Attempt to finnish the loan
		    System.out.print("Give the title of the DVD: ");
		    String title = input.nextLine();
		    lender.lendout(title, borrower);
		  }
	      }
	    System.out.println();
	  }

	else if(choice == 4)
	  {
	    // read the name
	    System.out.print("Give the name of the person you want to see(* for everyone): ");
	    String person = input.nextLine();
	    
	    // Print all persons overviews
	    if(person.equals("*"))
	      {
		for(String s : names.keySet())
		  {
		    Person current = names.get(s);
		    current.overview();
		  }
	      }
	    else
	      {
		// Check if person exists
		Person current = names.get(person);
		if(current == null)
		  {
		    System.out.println(person + " is not registered in the database!");
		  }
		else
		  {
		    // print persons overview
		    current.overview();
		  }
	      }
	    System.out.println();
	  }
	
	// Print all persons statistics
	else if(choice == 5)
	  {
	    for(String s : names.keySet())
	      {
		Person current = names.get(s);
		current.statistics();
		System.out.println();
	      }
	    System.out.println();
	  }

	else if(choice == 6)
	  {
	    System.out.println("Give the name of the person returning the DVD: ");
	    String name1 = input.nextLine();
	    Person borrower = names.get(name1);
	    if(borrower == null)
	      {
		System.out.println(name1 + " is not registered in the database");
	      }
	    else
	      {
		System.out.println("Give the title of the DVD: ");
		String title = input.nextLine();
		borrower.giveback(title);
	      }
	    System.out.println();
	  }
	else if(choice == 7)
	  {
	    System.out.println("TERMINATING PROGRAM");
	  }
      }while(choice != 7);
    input.close();
  }

  /**
     Method for reading the archive file, the method stores the names
     in the HashMap navneliste and ads data about each persons loaned
     DVD-s.
     
   */
  static void readarchive(HashMap<String, Person> names) throws Exception
  {
    String filename = "dvdarchive.txt";
    File file = new File(filename);
    Scanner reader = new Scanner(file);
    
    // Read the file line by line and add names
    // to the archive. Stop when '-' is reached
    while(reader.hasNextLine())
      {
	String name = reader.nextLine();
	if(name.startsWith("-"))
	  {
	    break;
	  }
	else
	  {
	    names.put(name, new Person(name));
	  }	
      }
    
    // Add DVDs to persons
    String init = reader.nextLine();
    Person current = names.get(init);

    while(reader.hasNextLine())
      {
	String line = reader.nextLine();

	// If line is "-", update the current person
	if(line.equals("-"))
	  {
	    current = names.get(reader.nextLine());
	  }

	// If line starts with "*", update loan and borrowing lists
	else if(line.startsWith("*"))
	  {
	    // get title and create new DVD
	    String title = line.substring(1, line.length());
	    DVD currentdvd = new DVD(title, current);
	    current.buy(currentdvd);

	    // Get the borrower and register the loan	    
	    Person borrower = names.get(reader.nextLine());
	    current.lendout(title, borrower);
	  }

	// Add DVD to current person
	else
	  {
	    DVD currentdvd = new DVD(line, current);
	    current.buy(currentdvd);
	  }
      }
    reader.close();
  }

  /**
     Method for writing changes done in the archive to the archive file
   */
  static void writearchive(HashMap<String, Person> names) throws Exception
  {
    PrintWriter out = new PrintWriter("dvdarchive.txt");

    // Write all names to file
    for(String s : names.keySet())
      {
	Person current = names.get(s);
	out.println(current.name());
      }

    // Write owned DVD-s to file
    for(String s : names.keySet())
      {
	Person person = names.get(s);
	out.println("-");
	out.println(s);

	// Write owned DVD-s to file
	HashMap<String, DVD> owned = person.owned();
	for(String title : owned.keySet())
	  {
	    DVD dvd = owned.get(title);
	    Person lender = dvd.borrower();
	    if(lender == null)
	      {
		out.println(dvd.title());
	      }
	  } 

	// Write dvds that are lended out with name of lender
	HashMap<String, DVD> lended = person.lended();
	for(String lendedout : lended.keySet())
	  {
	    DVD dvd = lended.get(lendedout);
	    Person borrower = dvd.borrower();
	    if(borrower != null)
	      {
		out.println("*" + dvd.title());
		out.println(borrower.name());
	      }
	  }
      }
    out.close();
  }
}

