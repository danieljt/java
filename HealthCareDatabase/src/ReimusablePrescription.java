
/**
 * Defines a reimusable prescription. The class inherits from the super class
 * Prescription. This prescription has it's price reduced by a percentage.
 * @author Daniel James Tarplett
 *
 */
public class ReimusablePrescription extends Prescription{
               public double percentage;
               public double price;
               
       public ReimusablePrescription(int id, int uses, Drug drug, Doctor doctor, Patient patient,
                       double percentage){
               super(id, uses, drug, doctor, patient);
               this.percentage = percentage; 
               // Calculate subsidized price
               this.price = drug.price*(1 - percentage/100);
       }
}
