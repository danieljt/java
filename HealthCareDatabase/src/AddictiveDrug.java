
/**
 * Class for defining an addictive drug. The addictiveness of the drug
 * is classified by an integer where 0 is trace.
 * @author daniel
 *
 */
public class AddictiveDrug extends Drug{
       protected int strength;
       
       public AddictiveDrug(int id, String name, int strength){
               super(id, name);
               this.strength = strength;
       }
       
       /**
        * Returns the strength of the drug
        * @return Addictiveness factor
        */
       public int strength(){
               return strength;
       }
}

