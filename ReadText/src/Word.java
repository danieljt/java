public class Word
{
  private String word;
  private int counter;

  public Word(String text)
  {
    // Add word, set counter to 1
    word = text;
    counter = 1;
  }

  public String toString()
  {
    // Return the word
    return word;
  }

  public int amount()
  {
    return counter;
  }

  public void increase()
  {
    counter++;
  }
}
