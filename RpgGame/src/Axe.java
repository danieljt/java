/**
 * Class for defining an axe. The class derives
 * from the Weapon class
 * @author Daniel James Tarplett
 *
 */
public class Axe extends Weapon{
    
    public Axe(String name, float weight, float[] scaling, int attackPower, float attackSpeed){
	super(name, weight, scaling, attackPower, attackSpeed);
	sortingParameter = 1; 
    }
}
