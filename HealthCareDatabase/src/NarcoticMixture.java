
/**
 * Class for defining a narcotic mixture drug. The class inherits from the 
 * superclass NarcoticDrug.
 * @author daniel
 *
 */
public class NarcoticMixture extends NarcoticDrug{
       protected double volume;
       protected double substance;
       
       public NarcoticMixture(int id, String name, double price, int strength, double volume,
                       double substance){
               super(id, name, strength);
               this.price = price;
               this.volume = volume;
               this.substance = substance;
       }
       
       /**
        * Returns the volume of the mixture in cm3
        * @return the volume in cm3
        */
       public double volume(){
               return volume;
       }
       
       /**
        * Returns the active substance per cm3 in the mixture 
        * @return the active substance per cm3
        */
       public double substance(){
               return substance;
       }
       
       /**
        * Returns the price of the mixture
        * @return price
        */
       public double price(){
               return price;
       }
}
