
/**
 * Class for keeping a list of objects.
 * @author daniel
 *
 * @param <E> Generic object to put in the list
 */
public class Tabel<E> {
       protected E[] tabel;
       protected int size;
       
       public Tabel(int size){
               this.size = size;
               tabel = (E[]) new Object[size];
       }

       /**
        * Method adds an element to the list at the indexed spot.
        * Program checks if the index is valid.
        * Program checks if the spot is vacant. 
        * If the spot is not valid, but within a reasonable distance from the
        * allowed range, the program increases the list size and adds the 
        * object to the list. If none of these conditions are met, the program gives
        * an error message.  
        * @param object Object to be added to list
        * @param index  The place in the list where the object is to be added
        */
       public void add(E object, int index){
    	   if(index - size > 20){
    		   System.out.println("The index specified is too large");
    	   }
    	   else if(index < 0){
    		   System.out.println("The index must be a positive integer");
    	   }
    	   else if(index - size <= 20 && index > size-1){
    		   E[] newtabel = (E[]) new Object[size+20];
    		   for(int i=0; i<size; i++){
    			   newtabel[i] = tabel[i];
    		   }
    		   tabel = newtabel;
    		   tabel[index] = object;
    		   size = size + 20;
    	   	}
    	   else if(tabel[index] != null){
    		   System.out.println("The spot specified is already taken");
    	   }
    	   else{
    		   tabel[index] = object;
    	   }
       }
       
       /**
        * Method returns the object specified in the list at the spot index.
        * If the index is is empty or out of range, the method returns null 
        * @param index the place where the object is
        * @return The object in the specified spot 
        */
       public E find(int index){
    	   return tabel[index];
       }
}
