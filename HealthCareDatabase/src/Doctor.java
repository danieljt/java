/**
 * Class for holding information about a doctor.
 * @author daniel
 *
 */
public class Doctor implements Comparable<Doctor>{
       protected int id;
       protected String firstName;
       protected String lastName;
       
       public Doctor(int id, String firstName, String lastName){
               this.id = id;
               this.firstName = firstName;
               this.lastName = lastName;
       }
       
       /**
        * Returns the doctors unique id
        * @return doctor id
        */
       public int id(){
               return id;
       }
       
       /**
        * returns the doctors name
        * @return doctors name
        */
       public String name(){
               return firstName + " " + lastName;
       }
       
       /**
        * First name of the doctor
        * @return first name
        */
       public String firstName(){
               return firstName;
       }
      
       /**
        * Last name of the doctor
        * @return last name
        */
       public String lastName(){
               return lastName;
       }
       
       /**
        * Prints non sensitive information about the doctor
        */
       public void printInfo(){
    	   System.out.println(lastName);
    	   System.out.println(id);
       }
       
       /**
        * Tests if two doctors have the same name
        * @param doctor the doctor to be compared to
        * @return true if the names are equal, false if they are not
        */
       public Boolean equals(Doctor doctor){
               if(compareTo(doctor) == 0)
               {
                       return true;
               }
               else{
                       return false;
               }
       }
       /**
        * Tests the full name of this doctor to the name of another doctor.
       * First, the surnames are tested, if they are equal, the first
        * names are tested. This crates an alphabetical order with respect
        * to surnames. 
        * @param doctor the doctor to be compared to
        * @return integer representing the alphabetical order of the names  
        */
      public int compareTo(Doctor doctor){    
               if(lastName.compareTo(doctor.lastName()) == 0){
                       return firstName.compareTo(doctor.firstName());
               }
               else{
                       return lastName.compareTo(doctor.lastName());
               }
       }
}

