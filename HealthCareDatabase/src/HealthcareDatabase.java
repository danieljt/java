
/**
 * Main program for running the health care database. The program runs a 
 * command line interface for automatic input and output. 
 * @author Daniel James Tarplett
 *
 */
public class HealthcareDatabase {
       protected FIFOprescriptionList fifo;
       protected LIFOprescriptionList lifo;
       protected Tabel<Patient> patients;
       protected Tabel<Drug> drugs;
       protected SortedList<Doctor> doctors;
       
       public static void main(String[] args) {
          
       }

}
