/**
 * Class for defining a hammer. The hammer class is a wrap class
 * for all blunt weapons like clubs, hammers and maces
 * @author Daniel James Tarplett
 *
 */
public class Hammer extends Weapon{
    public Hammer(String name, float weight, float[] scaling, int attackPower, float attackSpeed){
	super(name, weight, scaling, attackPower, attackSpeed);
	sortingParameter = 3; 
    }
}
