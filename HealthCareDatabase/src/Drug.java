
/**
 * Class for defining a drug. The drug has a unique id, 
 * a name and and a price. The price is dependent on how the 
 * drug is processed, and is not defined here. The price is defined in
 * the subclasses of this class.
 *  
 * @author Daniel James Tarplett
 *
 */
public class Drug {
	protected int id;
	protected String name;
	protected double price;
       
	public Drug(int id, String name){
		this.id = id;
		this.name = name;
	}
      
	/**
	 * Returns the id of the drug
	 * @return the id of the drug
	 */
	public int id(){
		return id;
	}
       
	/**
	 * Method for returning the name of the drug
	 * @return the name of the drug
	 */
	public String name(){
		return name;
	}
       
	public double price(){
		return price;
	}
	
	/**
	 * Prints info about the drug to terminal
	 */
	public void printInfo(){
		System.out.println(name);
	}
}
