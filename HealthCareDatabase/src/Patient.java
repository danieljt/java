
/**
 * Class for defining a patient. 
 * @author daniel
 *
 */
public class Patient {
       protected int id;
       protected String firstName;
       protected String lastName;
       protected String streetName;
       protected int streetNumber;
       protected int postcode;
       protected LIFOprescriptionList prescriptions;
       
       public Patient(int id, String firstName, String lastName, String streetName,
                       int streetNumber, int postcode){
               this.id = id;
               this.firstName = firstName;
               this.lastName = lastName;
               this.streetName = streetName;
               this.streetNumber = streetNumber;
               this.postcode = postcode;
               prescriptions = new LIFOprescriptionList();
       }
       
       /**
        * Adds a prescription to this persons prescription list
        * @param prescription
        */
       public void addPrescription(Prescription prescription){
    	   prescriptions.add(prescription);
       }
       
       /**
        * Prints info about the patient to terminal
        */
       public void printInfo(){
    	   System.out.println(firstName);
    	   System.out.println(lastName);
    	   System.out.println(streetName + " " + streetNumber);
    	   System.out.println(postcode);
    	   
       }
}
