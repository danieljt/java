/**
 * Class for defining a mixture of a drug without strong substances
 * @author daniel
 *
 */
public class RegularMixture extends RegularDrug{
       protected double volume;
       
       public RegularMixture(int id, String name, double price, double volume,
                       double substance){
               super(id, name);
               this.price = price;
               this.volume = volume;
       }
       
       /**
        * Returns the volume of the mixture in cm3
        * @return volume in cm3
        */
       public double volume(){
               return volume;
       }
       
       /**
        * Returns the price of the drug
        * @return price 
        */
       public double price(){
               return price;
       }
}

