
public class Reader
{
  public static void main(String[] args) throws Exception
  {
    // Define new dictionary and read text
    Dictionary file = new Dictionary();
    file.readText("example.txt");

    // Get the largest word and print
    Word max = file.max();
    System.out.println(max.toString() + ": " + max.amount());
     
  }
}
