
/**
 * Class for representing a narcotic drug. The class inherits from it's
 * superclass Drug, and needs this class to function. A narcotic drug is
 * characterized by it's strength, which is defined by the int value strength
 * @author daniel
 *
 */
public class NarcoticDrug extends Drug{
       protected int strength;
      
       public NarcoticDrug(int id, String name, int strength){
               super(id, name);
               this.strength = strength;
       }
       
       /**
        * Returns the strength of the drug
        * @return the strength of the drug
        */
       public int strength(){
               return strength;
       }
}

