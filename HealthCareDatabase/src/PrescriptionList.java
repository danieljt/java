import java.util.*;
/**
 * Class for holding a list of prescriptions. The list is a linked list,
 * and stores values as they come. The class is a superclass for the two lists
 * FIFIprescriptionList and LIFOprescriptionList
 * @author Daniel James Tarplett
 *
 */
public class PrescriptionList implements Iterable<Prescription>{
       protected Node head;
       protected Node tail;
       
       public PrescriptionList(){
               head = null;
               tail = null;
       }
       
       /**
        * Searches the list for the prescription and returns the prescription
        * @param id The id of the prescription
        * @return The prescription
        */
       public Prescription find(int id){
               Node current = head;
               while(current != null){
                       if(current.prescription.id() == id){
                               return current.prescription;
                       }
                       current = current.next;
               }
               return null;
       }
       
       /**
        * Applies new iterator
        */
       public Iterator<Prescription> iterator(){
    	   return new ListIterator();
       }
       
       /**
        * Class for holding nodes. The nodes have a prescription and a 
        * reference to their neighboring nodes.
        * @author Daniel James Tarplett
        *
        */
       protected class Node{
               Prescription prescription;
               Node next = null;
               
               Node(Prescription prescription){
            	   this.prescription = prescription;
               }
       }
       
       /**
        * Class for defining an iterator
        * @author Daniel James Tarplett
        *
        */
       protected class ListIterator implements Iterator<Prescription>{
    	   Node currentNode = head;
    	   
    	   public boolean hasNext(){
    		   return (currentNode != null);
    	   }
    	   
    	   public Prescription next(){
    		   Prescription currentObject = currentNode.prescription;
               currentNode = currentNode.next;
               return currentObject;
    	   }
    	   
    	   public void remove(){
    		   
    	   }
       }
}
