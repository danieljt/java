import java.util.*;
/**
* Class for implementing a list of objects E. The objects E must be extendible from
 * the superclass Comparable. The objects in the list are sorted alphabetically 
 * with respect to their variable names. The list members are added to special internal
 * Node objects to form a linked list.
 * @author daniel
 *
 * @param <E>
 */
public class SortedList<E extends Comparable<E>> implements Iterable<E>{
       private Node head;
       
       public SortedList(){
               head = null;
       }
       
       /**
        * Adds the object to the list. The method iterates through the list until it reaches
        * the element that is larger. The new element is then added before the larger
        * element, and linked to the element in front 
        * @param object The object to be added to the list
        */
       public void add(E object){
               Node node = new Node(object);
               
               // Empty list
               if(head == null){
                       head = node;
               }
               // Special case for the head
               else{
                       Node current = head;
                       
                       // Add in front of head if current element is smaller
                       if(object.compareTo(head.object) < 0){
                               node.next = current;
                               head = node;
                       }
                       // Else iterate through the list
                       else{
                               Node previous = head;
                               while(current != null){
                                       // Check is the current object is larger than the new object
                                       // If true, add the new object in front
                                       if(object.compareTo(current.object) < 0){
                                               node.next = current;
                                               previous.next = node;
                                               return;
                                       }
                                       // If the new object is the same, do nothing and exit the loop
                                       // without adding the new element to the list
                                       else if(object.compareTo(current.object) == 0){
                                               return;
                                       }
                                       // Update values for next iteration 
                                       previous = current;
                                       current = current.next;
                               }
                               // Add object to the end of the list when no other object is larger.
                               current = node;
                               previous.next = current;
                       }
               }
       }

       
       public Iterator<E> iterator(){
    	   return new ListIterator();
       }
       /**
        * Internal class for constructing a node. The node hold the object E, and
        * is linked to the next neighboring node. 
        * @author daniel
        *
        */
       private class Node{
               E object;
               Node next = null;
               
               Node(E object){
                       this.object = object;
               }
       }
       
       /**
        * Private class for defining the iterator as defined by the iterator interface
        * @author daniel
        *
        */
       private class ListIterator implements Iterator<E>{
               Node currentNode = head;
               public boolean hasNext(){
                       return (currentNode != null);
               }
               
               public E next(){
                       E currentObject = currentNode.object;
                       currentNode = currentNode.next;
                       return currentObject;
               }
               
               public void remove(){
                       
               }
       }
}

