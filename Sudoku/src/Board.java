/**
 * Class for constructing a rectangular board
 * @author Daniel James Tarplett
 *
 */
public class Board {
	public int boxRows;
	public int boxCols;
	public int boardLength;
	public Square[][] squares;
	public Column[] columns;
	public Row[] rows;
	public Box[] boxes;
	
	/**
	 * Method initializes all data on the board.
	 */
	public void initialize(int boxRows, int boxCols){
		this.boxRows = boxRows;
		this.boxCols = boxCols;
		this.boardLength = boxRows*boxCols;
		squares = new Square[boardLength][boardLength];
		columns = new Column[boardLength];
		rows = new Row[boardLength];
		boxes = new Box[boardLength];
		
		// Create columns, rows and boxes
		for(int i=0; i<boardLength; i++){
			columns[i] = new Column(i);
			rows[i] = new Row(i);
			boxes[i] = new Box(i);
		}
		
		// Add squares to the row, column and box lists. Link squares
		// together
		int counter = 0;
		int k = 0;
		for(int i=0; i<boardLength; i++){
			for(int j=0; j<boardLength; j++){
				k = i/boxRows*boxRows + j/boxCols;
				Square square = new Square(counter, rows[i], columns[j], boxes[k]);
				
				squares[i][j] = square;
				rows[i].add(square);
				columns[j].add(square);
				boxes[k].add(square);
				counter++;
			}
		}
	}
	
	/**
	 * Gets a square from the board
	 * @param RowNumber The row where the square is located
	 * @param colNumber The column where the square is located
	 * @return Square object 
	 */
	public Square getSquare(int RowNumber, int colNumber){
		return squares[RowNumber][colNumber];
	}
	
	/**
	 * Adds a value to a square on the board
	 */
	public void addSquare(int value, int RowNumber, int colNumber){
		squares[RowNumber][colNumber].addValue(value);
	}
	
	/**
	 * Prints the the board to the terminal
	 */
	public void print(){
		PrintBoard printer = new PrintBoard();
		printer.print();
	}
	
	/**
	 * The square class holds information about a square in the box. This includes
	 * it's column, row, box, value and id. 
	 * @author Daniel James Tarplett
	 */
	private class Square{
		private int id;
		private int value;
		private Row row;
		private Column column;
		private Box box;
		
		private Square(int id, Row row, Column column, Box box){
			this.id = id;
			this.row = row;
			this.column = column;
			this.box = box;
			this.value = 0;
		}
		
		public void addValue(int value){
			this.value = value;
		}

		/**
		 * Method finds all possible values this square can hold and returns an array
		 * with these values. The arrays length is always equal to the board length, where
		 * the value 0 is used to fill in the spots where the non possible values are. 
		 */
		public int[] possibleValues(){
			int[] possible = new int[boardLength];
			
			// Iterate the row, column and box for possible values. If 
			// the value is not found in any of them, it is added to the 
			// possible values list
			for(int i=1; i<boxRows*boxCols+1; i++){
				if(row.hasValue(i) == false && column.hasValue(i) == false && box.hasValue(i) == false){
					possible[i-1] = i;
				}
			}
			return possible;
		}
		
		public int value(){
			return value;
		}		
	}
	
	/**
	 * The column class holds information about the columns on the
	 * board.
	 * @author Daniel James Tarplett
	 *
	 */
	private class Column{
		private int id;
		private int counter;
		private Square[] squares;
		
		private Column(int id){
			this.id = id;
			counter = 0;
			squares = new Square[boardLength];
		}
		
		/**
		 * Tests if an integer value is stored in the column.
		 * Returns true if the value is in the column, and
		 * false if it is not.
		 * @return
		 */
		private Boolean hasValue(int value){
			for(Square square : squares){
				if(square.value() == value){
					return true;
				}
			}
			return false;
					
		}
		
		/**
		 * Adds a square to the column
		 * @param square
		 */
		private void add(Square square){
			squares[counter] = square;
			counter++;
			
		}
	}
	
	/**
	 * The row class holds information about each row on the board.
	 * It also holds ...
	 * @author Daniel James Tarplett
	 *
	 */
	private class Row{
		private int id;
		private int counter;
		private Square[] squares;
		
		private Row(int id){
			this.id = id;
			counter = 0;
			squares = new Square[boardLength];
		}
		
		/**
		 * Tests if an integer value is stored in the row.
		 * Returns true if the value is in the row, and
		 * false if it is not.
		 * @return
		 */
		private Boolean hasValue(int value){
			for(Square square : squares){
				if(square.value() == value){
					return true;
				}
			}
			return false;
		}
		
		/**
		 * Adds a square to the column
		 * @param square
		 */
		private void add(Square square){
			squares[counter] = square;
			counter++;
		}
	}
	/**
	 * The box class holds information about the boxes on the board.
	 * It also holds ...
	 * @author Daniel James Tarplett
	 *
	 */
	private class Box{
		private int id;
		private int counter;
		private Square[] squares;
		
		private Box(int id){
			this.id = id;
			counter = 0;
			squares = new Square[boardLength];
		}
		
		/**
		 * Tests if an integer value is stored in the box.
		 * Returns true if the value is in the box, and
		 * false if it is not.
		 * @return
		 */
		private Boolean hasValue(int value){
			for(Square square : squares){
				if(square.value() == value){
					return true;
				}
			}
			return false;
		}	
		
		/**
		 * Adds a square to the column
		 * @param square
		 */
		private void add(Square square){
			squares[counter] = square;
			counter++;
		}
	}
	
	/**
	 * Class for printing out the board to the terminal in a more or
	 * less nice format. 
	 * @author Daniel James Tarplett
	 *
	 */
	private class PrintBoard{
		// The length of the largest number on the board
		int length = Integer.toString(boxRows*boxCols).length();
		
		/**
		 * Main method for printing the board. The method utilizes the other
		 * methods printRow and printSeparator.
		 */
		public void print(){
			int rowNumber = 0;
			
			// Print the first rows before the first separator
			for(int i=0; i<boxRows;i++){
				printRow(rowNumber);
				rowNumber++;
			}
			
			// Print the rest of the rows with separators
			for(int i=0; i<boxCols-1;i++){
				printSeparator();
				for(int j=0;j<boxRows;j++){
					printRow(rowNumber);
					rowNumber++;
				}
			}
		}
		
		/**
		 * Prints a row 
		 * @param rowNumber the row to be printed
		 */
		public void printRow(int rowNumber){
			
			int colNumber = 0;			
			// Print the first part of the row
			for(int i=0; i<boxCols; i++){
				if(getSquare(rowNumber, colNumber).value() == 0){
					for(int j=0; j<length;j++){
						System.out.print(" ");
					}
				}
				else{
					System.out.format("%"+length+"s",getSquare(rowNumber, colNumber).value());
				}
				colNumber++;
			}
			
			// Print the rest of the row
			for(int i=0;i<boxRows-1;i++){
				System.out.print("|");
				for(int j=0;j<boxCols;j++){
					if(getSquare(rowNumber, colNumber).value() == 0){
						for(int k=0;k<length;k++){
							System.out.print(" ");
						}
					}
					else{
						System.out.format("%"+length+"s",getSquare(rowNumber, colNumber).value());
					}
					colNumber++;
				}
			}
			System.out.println();			
		}
		
		/**
		 * Prints a separator to the terminal
		 */
		public void printSeparator(){
			// Print the first part of the separator
			for(int i=0; i<boxCols; i++){
				for(int j=0; j<length;j++){
					System.out.print("-");
				}
			}
			
			// Print the rest of the separator
			for(int i=0; i<boxRows-1; i++){
				System.out.print("+");
				for(int j=0;j<boxCols;j++){
					for(int k=0;k<length;k++){
						System.out.print("-");
					}
				}
			}
			System.out.println();
		}
	}
}
	
