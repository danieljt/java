/**
 * Class for defining a package of narcotic pills. The class inherits from the 
 * superclass NarcoticDrug. 
 * @author daniel
 *
 */
public class NarcoticPills extends NarcoticDrug{
       protected int quantity;
       protected double substance;
       
       public NarcoticPills(int id, String name, double price, int strength, int quantity, 
                       double substance){
               super(id, name, strength);
               this.price = price;
               this.quantity = quantity;
               this.substance = substance;
       }
       
       /**
        * Returns the number of pills in this package.
        * @return number of pills in this package
        */
       public int quantity(){
               return quantity;
       }
       
       /**
        * returns the amount of active substance per pill
        * @return substance per pill in cm3
        */
       public double substance(){
               return substance;
       }
       
       /**
        * returns the price of the drug
        * @return price
        */
       public double price(){
               return price;
       }
}
