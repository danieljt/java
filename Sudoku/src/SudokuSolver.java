import java.util.Scanner;
import java.io.File;

/**
 * Solves a sudoku problem by constructing a board and running solver 
 * algorithms.
 * @author Daniel James Tarplett
 *
 */
public class SudokuSolver {
	
	Board board = new Board();
	
	/**
	 * Reads board information from file and adds to the board. The 
	 * structure of the file must be exact or the method will return
	 * an exception
	 */
	public void readFile(String filename) throws Exception{
		File file = new File(filename);
		Scanner reader = new Scanner(file);
		
		// Read number of rows and columns per box from file
		// Eliminate trailing space by invoking nextLine.
		int boxRow = reader.nextInt();
		int boxCol = reader.nextInt();
		reader.nextLine();
		
		// Initialize board
		board.initialize(boxRow, boxCol);
		
		// Read rest of file line by line and add to the board
		int rowNumber = 0;
		while(reader.hasNextLine()){
			String line = reader.nextLine();
			if(line.startsWith("#")){
				for(int i=0; i<boxRow*boxCol; i++){
					line = reader.nextLine();
					board.addSquare(Integer.parseInt(line), rowNumber, i);
					
				}
				rowNumber++;
			}
		}	
		reader.close();
	}
	
	/**
	 * Prints the board to terminal
	 */
	public void printBoard(){
		board.print();
	}
	
	/**
	 * Solves the board using a brute force algorithm and backtracking
	 */
	public void solve(){
		
	}
}
