/**
 * Class for defining a sword. The class derives from 
 * the Weapon class
 * @author Daniel James Tarplett
 *
 */
public class Sword extends Weapon{
    
    public Sword(String name, float weight, float[] scaling, int attackPower, float attackSpeed){
	super(name, weight, scaling, attackPower, attackSpeed);
	sortingParameter = 2;
    }
}
