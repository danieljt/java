/**
 * The Item class defines a general item, an object that a 
 * character can hold in a list
 * @author Daniel James Tarplett
 *
 */
public class Item{
    public String name;
    public String description;
    public int sortingParameter;
    
    public Item(String name){
	this.name = name;
    }
    /**
     * Returns the name of the item
     * @return name
     */
    public String getName(){
	return name;
    }
    
    /**
     * Returns this items sorting parameter. The sorting parameter
     * decides how this item is sorted with respect to other
     * items with the lowest value first
     * @return sorting Parameter
     */
    public int getSortingParameter(){
	return sortingParameter;
    }
    
    /**
     * Method compares this item to another, and returns true if this items
     * sortingParameter is larger than the other. If not, it returns false
     * @param item
     * @return
     */
    public boolean largerThan(Item item){
	if(getSortingParameter() > item.getSortingParameter()){
	    return true;
	}
	else{
	    return false;
	}
    }
	
}