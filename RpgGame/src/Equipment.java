/**
 * Class for defining equipment used by the character. The method derives
 * from the Item class
 * @author Daniel James Tarplett
 *
 */
public class Equipment extends Item{
    public float weight;
    
    public Equipment(String name, float weight){
	super(name);
	this.weight = weight;
    }
}
