
/**
 * Class for defining addictive pills. The class inherits from the superclass
 * AddictiveDrug. The class has a quantity variable defining the amount of pills
 * in a package, and a substance variable defining the active substance per pill.
 * @author daniel
 *
 */
public class AddictivePills extends AddictiveDrug{
       protected int quantity;
       protected double substance;
       
       public AddictivePills(int id, String name, double price, int strength, int quantity,
                       double substance){
               super(id, name, strength);
               this.price = price;
               this.quantity = quantity;
               this.substance = substance;
               }
               /**
                * Returns the amount of pills 
                * @return number of pills
                */
               public int quantity(){
                       return quantity;
               }
               
               /**
                * Returns the active substance per pill
                * @return active substance in one pill
                */
               public double substance(){
                       return substance;
               }
               
               /**
                * Returns the price of the drug
                * @return price
                */
               public double price(){
                       return price;
               }
}
