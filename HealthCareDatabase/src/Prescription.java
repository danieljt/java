/**
 * Class for defining a prescription.
 * @author daniel
 *
 */
public class Prescription {
	public int id;
	public int uses;
	public Drug drug;
	public Doctor doctor;
	public Patient patient;
	public double price;
       
	public Prescription(int id, int uses, Drug drug, Doctor doctor, Patient patient){
		this.id = id;
		this.uses = uses;
		this.drug = drug;
		this.doctor = doctor;
		this.patient = patient;
	}

       
	/**
	 * Method for using one reit of this prescription. If the uses are already zero,
	 * the program terminates with an error.
	 */
	public void use(){
		if(uses < 1){
			System.out.println("The prescription is not valid");
		}	
		else{	
    		   uses--;
		}	
	}
       
	/**
	 * Id of the prescription
	 * @return id
	 */
	public int id(){
		return id;
	}
       
	/**
	 * The price of the prescription. By default, the price of the prescription
	 * is equal to the price of the drug
	 * @return price of the prescription
	 */
	public double price(){
		return price;
	}
    
	/**
	 * Prints info about the prescription to terminal
	 */
	public void printInfo(){
		patient.printInfo();
		doctor.printInfo();
		drug.printInfo();
	}
}

