
/**
 * Class for defining a mixture from an addictive drug. The class inherits from
 * the superclass AddictiveDrug, and has a total volume in cm3 and a substance amount
 * per cm3.
 * @author daniel
 *
 */
public class AddictiveMixture extends AddictiveDrug{
       protected double volume;
       protected double substance;
       
       public AddictiveMixture(int id, String name, double price, int strength, double volume,
                       double substance){
               super(id, name, strength);
               this.price = price;
               this.volume = volume;
               this.substance = substance;
       }
       
       /**
        * Returns the volume of the mixture
        * @return the volume in cm3 of this mixture
        */
       public double volume(){
               return volume;
       }
       
       /**
        * Returns the amount of active substance per cm3
        * @return active substance amount in cm3
        */
       public double substance(){
               return substance;
       }
}

