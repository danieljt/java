/**
 * Class for holding a first in first out prescription list.
 * The list contains the prescriptions sorted from oldest to youngest.
 * @author Daniel James Tarplett
 *
 */
public class FIFOprescriptionList extends PrescriptionList{
       public FIFOprescriptionList(){
               super();
       }
       
       /**
        * Method adds a prescription to the list
        * @param prescription the prescription to be added
        */
       public void add(Prescription prescription){
               Node current = new Node(prescription);
               
               // Empty list
               if(head == null){
                       head = current;
                       tail = current;
                       head.next = tail;
               }
               else{
                       tail.next = current;
                       tail = current;
               }
       }
}

