
/**
 * Class for holding a last in first out prescription list.
 * The list contains prescriptions sorted from newest to oldest.
 * @author Daniel James Tarplett
 *
 */
public class LIFOprescriptionList extends PrescriptionList{
       public LIFOprescriptionList(){
               super();
       }
       
       /**
        * Adds a prescription to the list.
        * @param prescription prescription to be added
        */
       public void add(Prescription prescription){
               Node current = new Node(prescription);
               current.next = head;
               head = current;         
       }

}
