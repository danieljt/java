import java.util.Scanner;
import java.util.HashMap;
import java.io.File;

public class Dictionary
{
  HashMap<String, Word> dictionary = new HashMap<>();

  /**
     Method for reading a text file and assigning all words
     to their own objects. Method adds the words and their 
     frequency in a hashmap.
   */
  public void readText(String filename) throws Exception
  {
    File file = new File(filename);
    Scanner reader = new Scanner(file);

    while(reader.hasNextLine())
      {
	// Read next line
	String line = reader.nextLine();

	// Split line into words
	String words[] = line.split("\\s+");

	// Iterate over the words in line and
	// compute to the dictionary
	for(String word : words)
	  {
	    Word current = dictionary.get(word);
	    if(current == null)
	      {
		dictionary.put(word, new Word(word));
	      }
	    else
	      {
		current.increase();
	      }
	  }
      }
    reader.close();
  }
  /**
     Method for returning the dictionary.
   */
  public HashMap<String, Word> get()
  {
    return dictionary;
  }

  /**
     Method for searching after a word. If the word
     exists, the object is returned, if not, returns
     null.
   */
  public Word find(String word)
  {
    return dictionary.get(word);
  }

  /**
     Method for finding the word with the most occurences. Only the
     first word with this value is registered if more words share the
     maximum occurences
   */
  public Word max()
  {
    Word max = new Word("thisisatestwordfortestingpurposes");
    for(String s : dictionary.keySet())
      {
	Word word = dictionary.get(s);
	int counter = word.amount();
	if(counter > max.amount())
	  {
	    max = word;
	  }
      } 
    return max;
  }
}
