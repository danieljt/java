This is a simple program for a dvd administration. Here, we can register new persons, new dvds and perform rentals, purchases and overviews of the situation. The program reads and writes changes to the file dvdarkiv.txt.

---------------------
Classes:
---------------------
DVDadmin:
Main class for the program. Here, all operations are performed in an interactiveloop in the terminal. 

Person:
Class for holding objekts of type person

DVD:
Class for objects of DVDs. 
