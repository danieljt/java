/**
 * Class for defining a weapon. the class derives from the
 * equipment class 
 * @author Daniel James Tarplett
 *
 */
public class Weapon extends Equipment{
    public float[] scaling;
    public int attackPower;
    public float attackSpeed;
    
    public Weapon(String name, float weight, float[] scaling, int attackPower, float attackSpeed){
	super(name,weight);
	this.scaling = scaling;
    }
    /**
     * The scaling list defines the different attributes that this 
     * weapon scales with
     * @return
     */
    public float[] scaling(){
	return scaling;
    }
    
    public int attackPower(){
	return attackPower;
    }
    
    public float attackSpeed(){
	return attackSpeed;
    }
}
