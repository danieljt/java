The package contains programs for constructing a type of pharmacy/hospital database. 
The database contains information about patients, doctors, drugs and prescriptions.
All information is stored in linked lists and arrays, and the user will be able to 
do tasks including inserting, deleting and many more operations.

HIERARCHY FOR DRUGS
----------------------------------------------------------------------------------------------------
Drug.java is the main class for drugs. It's subclasses are RegularDrug.java, AddictiveDrug.java 
and NarcoticDrug.java containing information about Regular drugs, drugs with addictive substances
and drugs with narcotic substances. Each of the subclasses have their own subclasses for pills and
mixtures respectively. RegularPills.java and RegularMixture.java are subclasses of RegularDrug.java
and so on. 

Drug.java is superclass for RegularDrug.java, AddictiveDrug.java, NarcotivDrug
RegularDrug.java is the superclass for RegularPills.java and RegularMixture.java   
AddictiveDrug.java is the superclass for AdictivePills.java and AddictiveMixture.java 
NarcoticDrug.java is the superclass for NarcoticPills.java and NarcoticMixture.java 

HIERARCHY FOR DOCTORS
----------------------------------------------------------------------------------------------------
Doctor.java is the main class for doctors. Its subclass is GeneralPractitioner.java for doctors
who have special contracts with their corresponding districts.

HIERARCHY FOR PRESCRIPTIONS
----------------------------------------------------------------------------------------------------
Prescription.java is the main class for prescriptions. Prescription has the subclasses 
RegularPrescription.java and ReimusablePrescription.java where the latter is a prescription that
is partially compensated by the government.

HIERARCHY FOR PATIENTS
----------------------------------------------------------------------------------------------------
The patient class holds important information about a patient. There are no subclasses in this
class




