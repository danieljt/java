
/**
 * Class for defining a standard drug without strong substance.
 * effects
 * @author daniel
 *
 */
public class RegularDrug extends Drug{
       public RegularDrug(int id, String name){
               super(id, name);
       }
}

