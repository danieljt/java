
/**
 * Class for the definition of a general practitioner. This class i a sub class of
 * the Doctor class. The general practitioner is a doctor with a government contract
 * with a unique contract number
 * @author Daniel James Tarplett
 *
 */
public class GeneralPractitioner extends Doctor{
       protected int contractNumber;
       
       public GeneralPractitioner(int id, String firstName, String lastName, int contractNumber){
               super(id, firstName, lastName);
               this.contractNumber = contractNumber;
       }
       
       /**
        * Returns the contract number for the general practitioner
        * @return
        */
       public int contractNumber(){
               return contractNumber;
       }
}

